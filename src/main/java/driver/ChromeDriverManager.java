package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverManager extends DriverManager {
	
	private static final String WEB_DRIVER_NAME = "webdriver.chrome.driver";
	private static final String FILE_NAME = "chromedriver";

	@Override
	protected String getWebDriverName() {
		return WEB_DRIVER_NAME;
	}

	@Override
	protected String getFileName() {
		return FILE_NAME;
	}

	@Override
	protected WebDriver getWebDriver() {
		return new ChromeDriver();
	}

}
