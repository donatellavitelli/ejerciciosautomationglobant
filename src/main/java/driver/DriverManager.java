package driver;

import org.openqa.selenium.WebDriver;

import java.io.File;

public abstract class DriverManager {
	
	private static final String BASE_PATH = "src/main/resources/"; 
	private static String OS = System.getProperty("os.name").toLowerCase();

    protected abstract String getWebDriverName();

    protected abstract String getFileName();

    protected abstract WebDriver getWebDriver();

	private String getOsFileName(String fileName){
		if(OS.contains("win"))
			return fileName + ".exe";
		if(OS.contains("mac"))
			return fileName + "Mac";
		if(OS.contains("nix") || OS.contains("nux") || OS.contains("aix"))
			return fileName + "_linux32";
		return null;
	}

	private String getPath(final String fileName) {
		return new File(BASE_PATH + fileName).getAbsolutePath();
	}

	public static WebDriver setUp(String browser) {
	    DriverManager driverManager = null;
	    if(browser.equals("firefox"))
	        driverManager = new FirefoxDriverManager();
	    if(browser.equals("chrome"))
	        driverManager = new ChromeDriverManager();

        String fileNameExtension = driverManager.getOsFileName(driverManager.getFileName());
        System.setProperty(driverManager.getWebDriverName(), driverManager.getPath(fileNameExtension));

        return driverManager.getWebDriver();
    }

}
