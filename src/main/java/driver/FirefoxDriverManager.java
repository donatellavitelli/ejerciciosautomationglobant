package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends DriverManager {
	
	private static final String WEB_DRIVER_NAME = "webdriver.gecko.driver";
	private static final String FILE_NAME = "geckodriver";

	@Override
	protected String getWebDriverName() {
		return WEB_DRIVER_NAME;
	}

	@Override
	protected String getFileName() {
		return FILE_NAME;
	}

	@Override
	protected WebDriver getWebDriver() {
		return new FirefoxDriver();
	}

}
