package travel.flies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import utils.BrowserUtils;

import java.util.concurrent.TimeUnit;

public class Fly {

	@FindBy(id = "tab-flight-tab-hp")
	private WebElement flightTab;
	
	@FindBy(id="flight-origin-hp-flight")
	private WebElement flightOrigin;
	
	@FindBy(id="flight-destination-hp-flight")
	private WebElement flightDestination;
	
	@FindBy(id="flight-departing-hp-flight")
	private WebElement flightDeparting;
	
	@FindBy(id="flight-returning-hp-flight")
	private WebElement flightReturning;
	
	@FindBy(css=".col.search-btn-col > button")
	private WebElement searchButton;

//	@FindBy(css="#flightModuleList > li:first-child button")
//	private WebElement searchButtonDepartureAndReturn;

    @FindBy(css="div.datepicker-cal-month")
    private WebElement firstMonth;

    @FindBy(css="div.datepicker-cal-month + div.datepicker-cal-month")
    private WebElement secondMonth;
	
	@FindBy(css="button.datepicker-paging.datepicker-next.btn-paging.btn-secondary.next")
	private WebElement monthButton;

    @FindBy(css=".flex-card.flex-tile.details.OD0 h3")
    private WebElement departingDateResult;

    @FindBy(css=".flex-card.flex-tile.details.OD1 h3")
    private WebElement returningDateResult;

    @FindBy(css=".title-city-text")
	private WebElement departureTextValidation;

	@FindBy(css=".title-city-text")
	private WebElement textValidation;

	@FindBy(css="select[name=\"sort\"]")
	private WebElement dropdownValue;

	@FindBy(css="#flightModuleList li:nth-child(3) button")
	private WebElement searchButtonDeparture;

	@FindBy(css="#flightModuleList li:nth-child(6) button")
	private WebElement searchButtonReturn;

	public void setSearchButtonDeparture(WebElement searchButtonDeparture) {
		this.searchButtonDeparture = searchButtonDeparture;
	}
	public void setSearchButtonReturn(WebElement searchButtonReturn) {
		this.searchButtonReturn = searchButtonReturn;
	}

	public WebElement getDepartingDateResult() {
        return departingDateResult;
    }

    public WebElement getReturningDateResult() {
        return returningDateResult;
    }

    public void goToPage(WebDriver driver){
		driver.get("https://www.travelocity.com/");
		BrowserUtils.maximizeTab(driver);
	}
	
    public void setDepartureAirport(WebDriver driver, String origin){
		BrowserUtils.waitElementToBeClickeable(driver, flightTab, 20);
		flightTab.click();
		flightOrigin.sendKeys(origin);
    }
    
    public void setReturningAirport(String destination){
		flightDestination.click();
    	flightDestination.sendKeys(destination);
    }
    
    public void setDepartureDate(WebDriver driver, String monthOrigin, String dayOrigin) throws InterruptedException{
    	flightDeparting.click();
		BrowserUtils.scrollDown(driver, 500);
        WebElement m = selectMonth(monthOrigin);
		selectDay(dayOrigin, m);
    }
    
    public void setReturningDate(String monthDestination, String dayDestination) throws InterruptedException{
    	flightReturning.click();
		WebElement m = selectMonth(monthDestination);
		selectDay(dayDestination, m);
    }

    public void searchFlights(){
		searchButton.click();
	}
    
    public void chooseDeparturingFlight(WebDriver driver, String condition){
		BrowserUtils.scrollDown(driver, 500);
		BrowserUtils.waitElementToBeClickeable(driver, dropdownValue,20);
		selectFromDropdown(condition);
		BrowserUtils.scrollDown(driver, 200);
        searchButtonDeparture.click();
    }
    
    public void chooseReturningFlight(WebDriver driver, String condition){
		BrowserUtils.waitElementToBeClickeable(driver, dropdownValue,20);
		selectFromDropdown(condition);
		BrowserUtils.scrollDown(driver, 400);
		searchButtonReturn.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

	public WebElement selectMonth(String month) throws InterruptedException{
        String cssClass = "table > caption.datepicker-cal-month-header";
	    if(firstMonth.findElement(By.cssSelector(cssClass)).getText().equals(month))
			return firstMonth;
		
		while(!secondMonth.findElement(By.cssSelector(cssClass)).getText().equals(month)){
			monthButton.click();
		}
		return secondMonth;
	}
	
	public void selectDay(String day, WebElement month){
        month.findElement(By.cssSelector("button[data-day=\"" + day + "\"]")).click();

	}

	public void selectFromDropdown(String condition){
		Select dropdown= new Select(dropdownValue);
		dropdown.selectByVisibleText(condition);
	}

	public WebElement getFlightOrigin() {
		return flightOrigin;
	}

	public WebElement getFlightDestination() {
		return flightDestination;
	}

	public WebElement getFlightDeparting() {
		return flightDeparting;
	}

	public WebElement getFlightReturning() {
		return flightReturning;
	}

	public WebElement getTextValidation() {return textValidation;}

}
	