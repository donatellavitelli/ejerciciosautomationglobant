package travel.flies;

public class TestData {
    public static final int ORIGIN = 0;
    public static final int DESTINATION = 1;
    public static final int TITLE_MAIN_PAGE = 2;
    public static final int MONTH_DEPARTURE = 3;
    public static final int MONTH_RETURN  = 4;
    public static final int DAY_DEPARTURE = 5;
    public static final int DAY_RETURN = 6;
    public static final int DATE_DEPARTURE_RESULT = 7;
    public static final int DATE_RETURNING_RESULT = 8;
    public static final int DATE_DEPARTURE_FINAL_RESULT = 9;
    public static final int DATE_RETURNING_FINAL_RESULT = 10;
    public static final int FLIGHT_DEPARTURE = 11;
    public static final int FLIGHT_RETURN = 12;
    public static final int TAB = 13;
    public static final int PRICE_DEPARTURE = 14;
    public static final int PRICE_RETURN = 15;
    public static final int TYPE_OF_FLIGHT_DEPARTURE = 16;
    public static final int TYPE_OF_FLIGHT_RETURN = 17;
}
