package travel.tests;

import driver.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import travel.flies.Fly;
import travel.flies.TestData;
import utils.BrowserUtils;
import utils.ExcelUtils;

public class Tests {
	
	private WebDriver driver;
	private Fly fly;
	
	@DataProvider(name = "TravelInfo")
	public static Object[][] testDataExistingCampaign(ITestContext context) throws Exception {
		String workbook = context.getCurrentXmlTest().getParameter("workbook");
		return ExcelUtils.getTableArray(System.getProperty("user.dir") + "/" + workbook,"Sheet1");
	}

    @Parameters("browser")
	@BeforeClass
	public void setDriver(String browser) {
	    driver = DriverManager.setUp(browser);
		fly = PageFactory.initElements(driver, Fly.class);
	}
	
	@Test(priority = 1, dataProvider = "TravelInfo")
	public void validatePageTitle(String ...params) throws InterruptedException{
		fly.goToPage(driver);
		Assert.assertEquals(driver.getTitle(), params[TestData.TITLE_MAIN_PAGE]);
	}
	
	@Test(priority = 2, dataProvider = "TravelInfo")
	public void validateDepartureAirport(String ...params){
		fly.setDepartureAirport(driver, params[TestData.ORIGIN]);
		Assert.assertEquals(fly.getFlightOrigin().getAttribute("value"), params[TestData.ORIGIN]);
	}
	
	@Test(priority = 3, dataProvider = "TravelInfo")
	public void validateReturningAirport(String ...params){
		fly.setReturningAirport(params[TestData.DESTINATION]);
		Assert.assertEquals(fly.getFlightDestination().getAttribute("value"), params[TestData.DESTINATION]);
	}
	
	@Test(priority = 4, dataProvider = "TravelInfo")
	public void validateDepartureDate(String ...params) throws InterruptedException{
		fly.setDepartureDate(driver, params[TestData.MONTH_DEPARTURE], params[TestData.DAY_DEPARTURE]);
		Assert.assertEquals(fly.getFlightDeparting().getAttribute("value"), params[TestData.DATE_DEPARTURE_RESULT]);
	}
	
	@Test(priority = 5, dataProvider = "TravelInfo")
	public void validateReturningDate(String ...params) throws InterruptedException{
		fly.setReturningDate(params[TestData.MONTH_RETURN], params[TestData.DAY_RETURN]);
		Assert.assertEquals(fly.getFlightReturning().getAttribute("value"), params[TestData.DATE_RETURNING_RESULT]);
	}

	@Test(priority = 6, dataProvider = "TravelInfo")
	public void validateDepartureFlightSelection(String ...params){
		fly.searchFlights();
		Assert.assertTrue(fly.getTextValidation().getText().contains(params[TestData.FLIGHT_DEPARTURE]));
	}
	
	@Test(priority = 7, dataProvider = "TravelInfo")
	public void validateReturningFlightSelection(String ...params){
		fly.chooseDeparturingFlight(driver, params[TestData.TYPE_OF_FLIGHT_DEPARTURE]);
		Assert.assertTrue(fly.getTextValidation().getText().contains(params[TestData.FLIGHT_RETURN]));
	}
	
	@Test(priority = 8, dataProvider = "TravelInfo")
	public void validatePurchase(String ...params){

		fly.chooseReturningFlight(driver, params[TestData.TYPE_OF_FLIGHT_RETURN]);

		BrowserUtils.getTab(driver, params[TestData.TAB]);
		Assert.assertEquals(fly.getDepartingDateResult().getText(), params[TestData.DATE_DEPARTURE_FINAL_RESULT]);
		Assert.assertNotEquals(fly.getReturningDateResult().getText(), params[TestData.DATE_RETURNING_FINAL_RESULT]);
	}

	@AfterSuite
    public void closeDriver() {
		driver.quit();
    }
}
