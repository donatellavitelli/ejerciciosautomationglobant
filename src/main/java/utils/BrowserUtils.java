package utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by gparis on 10/25/17.
 */
public class BrowserUtils {

    public static void getTab(WebDriver driver, final String tabTitle){
        for(String tab : driver.getWindowHandles()) {
            driver.switchTo().window(tab);
            if(driver.getTitle().equals(tabTitle))
                break;
        }
    }

    public static void waitElementToBeClickeable(WebDriver driver, WebElement flightTab, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(flightTab));
    }

    public static void scrollDown(WebDriver driver, int pixel){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("scroll(0," + pixel + ");");
    }

    public static void maximizeTab(WebDriver driver){
        driver.manage().window().maximize();
    }
}
