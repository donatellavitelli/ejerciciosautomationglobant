package utils;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	
	private static XSSFSheet xssfSheet;
	private static XSSFWorkbook xssfWorkbook;
	private static XSSFCell xssfCell;
	
	public static Object[][] getTableArray(final String file, final String sheet) throws IOException {
		FileInputStream excelFile = new FileInputStream(file);
		xssfWorkbook = new XSSFWorkbook(excelFile);
		xssfSheet = xssfWorkbook.getSheet(sheet);
		
		int rows = xssfSheet.getLastRowNum();
		int columns = xssfSheet.getRow(1).getLastCellNum();
		
		Object[][] data = new String[rows][columns];
		
		for(int i = 1; i <= rows; i ++)
			for(int j = 1; j <= columns; j ++)
				data[i-1][j-1] = getCellData(i, j);
		
		return data;
	}
	
	@SuppressWarnings("deprecation")
	public static String getCellData(final int row, final int column) {
		xssfCell = xssfSheet.getRow(row).getCell(column);
		if(xssfCell == null)
			return "";
		switch(xssfCell.getCellType()) {
			case XSSFCell.CELL_TYPE_NUMERIC:
				int number = (int) xssfCell.getNumericCellValue();
				return String.valueOf(number);
			case XSSFCell.CELL_TYPE_STRING:
				return xssfCell.getStringCellValue();
			case XSSFCell.CELL_TYPE_BLANK:
			default:
				return "";
		}
	}
}
